#!/bin/bash

while :
do
	#Get the user input in a variable called userinput
	read -p 'cuscmd# ' userinput;

	#If the command is ac
	if [ $userinput == "ac" ]; then
	#Display all Courses 
	disCourses () {
		echo ---------------------------------
		ls courses| grep "COMPX" 
		echo ---------------------------------
	}
	disCourses
	
	#If the command is myco
	elif [ $userinput == "myco" ]; then
	#Create a Directory for User Courses
	makeMyco() {
		cd courses
		mkdir mycourses
		echo ---------------------------------
		echo mycourses has been created!
		echo ---------------------------------
		cd
	}
	makeMyco

	#if the command is reco
	elif [ $userinput == "reco" ]; then
	#Remove the Directory for User Courses
	removeMyco() {
		cd courses
		rm -r -i mycourses
		echo ---------------------------------
		echo mycourses has been removed!
		echo ---------------------------------
		cd
	}
	removeMyco

	#if the command is mvco
	elif [ $userinput == "mvco" ]; then
	#Move the course to the mycourses Directory
	moveMyco() {
		cd courses
		ls
		echo ---------------------------------
		echo What folder do you wish to add to mycourses?
		echo ---------------------------------
		read varco
		mv -v $varco mycourses 
		#mv -v -t $varco mycourses 
		echo ---------------------------------
		echo $varco has been added to mycourses
		echo ---------------------------------
		cd
	}
	moveMyco

	#if the command is moco
	elif [ $userinput == "moco" ]; then
	#move the courses out of mycourses Directory
	removeCo() {
		cd courses/mycourses
		echo ---------------------------------
		echo What folder do you wish to remove from mycourses?
		echo ---------------------------------
		read varco
		mv -v $varco ../ 
		echo ---------------------------------
		echo $varco has been removed from mycourses
		echo ---------------------------------
		cd
	}
	removeCo

	#if the command is lsco
	elif [ $userinput == "lsco" ]; then
	#List users courses
	listCo() {
		echo ---------------------------------
		echo These are your courses!
		echo ---------------------------------
		ls courses/mycourses
		echo ---------------------------------
	}
	listCo

	#If the command is end
	elif [ $userinput == "end" ]; then
	#End script
	end() {
		echo ---------------------------------
		echo "Thanks for using the Program!"
		echo ---------------------------------
		exit
	}
	end

	#If the command is st
	elif [ $userinput == "st" ]; then
	#Display Sub Topics
	subTopic() {
		cd courses
		echo ---------------------------------
		echo Enter your Course?
		echo ---------------------------------
		read varco
		echo ---------------------------------
		ls $varco
		echo ---------------------------------
		cd
	}
	subTopic
	
	#If the command is chp
	elif [ $userinput == "chp" ]; then
	#Display Chapter
	displayChap() {
		cd courses
		echo ---------------------------------
		echo Enter what course?
		echo ---------------------------------
		read varco
		echo ---------------------------------
		echo Enter what week you wish to view?
		echo ---------------------------------
		read varwe
		echo ---------------------------------
		ls $varco/$varwe
		echo ---------------------------------
		#ls /home/uowstudent/courses/$varco/$varwe
		cd
	}
	displayChap

	#If the command is disdir
	elif [ $userinput == "disdir" ]; then
	#Display the Tree Directory to the User
	displayCoTree() {
		cd courses
		echo ---------------------------------
		find | ls -l
		#find /home/uowstudent/courses | ls -l
		echo ---------------------------------
		cd
	}
	displayCoTree

	#if the command is disco
	elif [ $userinput == "disco" ]; then
	#Display the Directory for mycourses
	displayMycoTree() {
	      	cd courses/mycourses
		echo ---------------------------------
		find | ls -l
		#find /home/uowstudent/courses/mycourses | ls -l
		echo ---------------------------------
		cd
	}
	displayMycoTree
	
	#if the command is diskw
	elif [ $userinput == "diskw" ]; then
	#Display content based on user input
	displayKeyword() {
		echo ---------------------------------
		echo Please enter a keyword/number combination?
		echo ---------------------------------
		read varin
		cd courses 
		echo ---------------------------------
		find | grep "$varin"
		echo ---------------------------------
		cd
	}
	displayKeyword
	
	#if the command is refi
	elif [ $userinput == "refi" ]; then
	#Reads file content in a certain place.
	readfile() {
		cd courses
		echo ---------------------------------
		echo Please enter what file you want to access?
		echo ---------------------------------
		echo Which course?
		echo ---------------------------------
		ls | grep "COMPX"
		echo ---------------------------------
		read varco
		echo ---------------------------------
		echo which Subtopic?
		echo ---------------------------------
		read varwe
		cd $varco
		echo ---------------------------------
		ls
		echo ---------------------------------
		echo which file?
		echo ---------------------------------
		read varfi
		cd $varwe
		echo ---------------------------------
		echo This is the information required:
		echo ---------------------------------
		echo "$(cat $varfi)"
		echo ---------------------------------
		cd
	}
	readfile
	
	#if the command is cofi
	elif [ $userinput == "cofi" ]; then
	#Reads file content in a certain place.
	readMycoFiles() {
		cd courses/mycourses
		echo ---------------------------------
		echo Please enter what file you want to access?
		echo ---------------------------------
		echo Which course?
		echo ---------------------------------
		ls | grep "COMPX"
		echo ---------------------------------
		read varco
		echo ---------------------------------
		echo which Subtopic?
		echo ---------------------------------
		read varwe
		cd $varco
		echo ---------------------------------
		ls
		echo ---------------------------------
		echo which file?
		echo ---------------------------------
		read varfi
		cd $varwe
		echo ---------------------------------
		echo This is the information required:
		echo ---------------------------------
		echo "$(cat $varfi)"
		echo ---------------------------------
		cd
	}
	readMycoFiles

	#if the command is man
	elif [ $userinput == "man" ]; then
	#Reads file content in the Manual.
	manual() {
		cd courses
		echo ---------------------------------
		echo "$(cat Cuscmd_Manual.txt)"
		echo ---------------------------------
		cd 
	}
	manual

	#if the command is cf
	elif [ $userinput == "cf" ]; then
	#User can cd into that folder.
	cdfolder() {
		cd courses
		echo ---------------------------------
		echo Please enter what course you want to go into?
		echo ---------------------------------
		ls 
		echo ---------------------------------
		read varfo
		echo ---------------------------------
		cd $varfo 
		echo Want to view Sub Topic? [yes or no]
		echo ---------------------------------
		ls 
		echo ---------------------------------
		read varst
			if [ $varst == "yes" ]; then
			echo ---------------------------------
			echo what week do you want to go to?
			echo ---------------------------------
			read varwe	
			echo ---------------------------------
			cd $varwe | ls
			echo ---------------------------------
			
			else
			echo ---------------------------------
			fi
	}
	cdfolder

	#if the command is cfo
	elif [ $userinput == "cfo" ]; then
	#User can cd into back to courses.
	cdhome() {
		echo ---------------------------------
		cd 
		ls 
		echo ---------------------------------
	}
	cdhome
	
	#if the command is back
	elif [ $userinput == "back" ]; then
	#User can cd into back to courses.
	cdback() {
		echo ---------------------------------
		cd ..  
		ls 
		echo ---------------------------------
	}
	cdback

	#If the command is unknown display an error message
	else
        echo "Error: command" $userinput "does not exist."
	fi
done


